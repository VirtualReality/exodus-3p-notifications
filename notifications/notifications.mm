/**
 * @file notifications.mm
 * @brief Objective-C++ library for sending notifications to Notification Center
 *
 * $LicenseInfo:firstyear=2012&license=mit$
 * Copyright (C) 2012 Katharine Berry
 *
 * Permission is hereby granted, free of charge, to any person obtaining
 * a copy of this software and associated documentation files (the
 * "Software"), to deal in the Software without restriction, including
 * without limitation the rights to use, copy, modify, merge, publish,
 * distribute, sublicense, and/or sell copies of the Software, and to
 * permit persons to whom the Software is furnished to do so, subject to
 * the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
 * CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
 * TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
 * SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 * $/LicenseInfo$
 */

#include <Cocoa/Cocoa.h>
#include "notifications.h"
#include "KBNotificationDelegate.h"

namespace notifications {
	bool exists() {
		return (NSClassFromString(@"NSUserNotification") != nil);
	}
	
	void prepare() {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		if(exists()) {
			KBNotificationDelegate *delegate = [[KBNotificationDelegate alloc] init];
			[[NSUserNotificationCenter defaultUserNotificationCenter] setDelegate:delegate];
		}
		[pool release];
	}
	
	void set_activation_callback(activated_function callback) {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		if(exists()) {
			((KBNotificationDelegate*)[NSUserNotificationCenter defaultUserNotificationCenter].delegate).callback = callback;
		}
		[pool release];
	}
	
	bool notify(std::string title, std::string subtitle, std::string informative_text) {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		if(exists())
		{
			NSUserNotification *notification = [[NSUserNotification alloc] init];
			if(!title.empty()) {
				notification.title = [NSString stringWithUTF8String:title.c_str()];
			}
			if(!subtitle.empty()) {
				notification.subtitle = [NSString stringWithUTF8String:subtitle.c_str()];
			}
			if(!informative_text.empty()) {
				notification.informativeText = [NSString stringWithUTF8String:informative_text.c_str()];
			}
			
			[[NSUserNotificationCenter defaultUserNotificationCenter] deliverNotification:notification];
			[notification release];
			[pool release];
			return true;
		}
		else
		{
			[pool release];
			return false;
		}
	}
	
	void clear_all() {
		NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];
		if(exists()) {
			[[NSUserNotificationCenter defaultUserNotificationCenter] removeAllDeliveredNotifications];
		}
		[pool release];
	}
}
