#!/bin/bash

cd "$(dirname "$0")"

# turn on verbose debugging output.
set -x
# make errors fatal
set -e

if [ -z "$AUTOBUILD" ] ; then 
    fail
fi

if [ "$OSTYPE" = "cygwin" ] ; then
    export AUTOBUILD="$(cygpath -u $AUTOBUILD)"
fi

NOTIFICATIONS_VERSION="1"
NOTIFICATIONS_SOURCE_DIR="notifications"

# load autobuild provided shell functions and variables
eval "$("$AUTOBUILD" source_environment)"

top="$(cd $(dirname $0) ; pwd)"
stage="$(pwd)/stage"

pushd "$NOTIFICATIONS_SOURCE_DIR"
    case "$AUTOBUILD_PLATFORM" in
        "windows")
            fail "Notifications only targets OS X"
        ;;
        "darwin")
            xcodebuild

            mkdir -p "$stage/lib/release"
            mkdir -p "$stage/include/notifications"
            mkdir -p "$stage/LICENSES"
            cp -R "build/Release/libnotifications.dylib" "$stage/lib/release/"
            cp "notifications.h" "$stage/include/notifications/"
            cp LICENSE.txt "$stage/LICENSES/exodus-notifications.txt"
        ;;
        "linux")
            fail "Notifications only targets OS X"
        ;;
    esac
popd

pass